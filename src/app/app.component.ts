import { Component, OnInit } from '@angular/core';

declare var PIXI:any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'app';

  //Variables para manejo de PIXI
  public app: any;
  Application = PIXI.Application;
  Container = PIXI.Container;
  loader = PIXI.loader;
  resources = PIXI.loader.resources;
  TextureCache = PIXI.utils.TextureCache;
  Sprite = PIXI.Sprite;
  Rectangle = PIXI.Rectangle;

  ngOnInit() {
    let type = "WebGL";
    if(!PIXI.utils.isWebGLSupported()){
      type = "canvas";
    }
    PIXI.utils.sayHello(type);

    var containerReference = document.getElementById("container");

    const canvas = document.createElement("canvas");
    canvas.id = "myCanvas";

    this.app = new this.Application({
          width: 1024,
          height: 576,
          view: canvas
    });

    containerReference.appendChild(this.app.view);
    // var canvasReference = document.getElementById("myCanvas");

    this.loader
      .load(this.Setup());
  }

  Setup(){
    var background = new this.Sprite.fromImage("../../assets/ui/background.jpg");
    background.width = 1024;
    background.height = 576;
    this.app.stage.addChild(background);

    let text = new PIXI.Text('Mockup',{fontFamily : 'Arial', fontSize: 100, fill : 0xffffff, align : 'center'});
    this.app.stage.addChild(text);

    var button = PIXI.Sprite.fromImage('../../assets/ui/buttonPlaceholder.png');
    button.anchor.set(0.5);
    button.scale.set(0.3);
    button.position.x = 512;
    button.position.y = 450;
    // Opt-in to interactivity
    button.interactive = true;
    // Shows hand cursor
    button.buttonMode = true;
    button.on('pointerdown', () => console.log('Probando botón 1'));
    this.app.stage.addChild(button);

    var button2 = PIXI.Sprite.fromImage('../../assets/ui/buttonPlaceholder2.png');
    button2.anchor.set(0.5);
    button2.scale.set(0.3);
    button2.position.x = 512;
    button2.position.y = 520;
    // Opt-in to interactivity
    button2.interactive = true;
    // Shows hand cursor
    button2.buttonMode = true;
    button2.on('pointerdown', () => console.log('Probando botón 2'));
    this.app.stage.addChild(button2);
    this.resize();
  }

  resize(){
    let container = document.getElementById("container");
    let form = document.getElementById("dataForm");
    let scale = ((100 * container.offsetWidth) / this.app.screen.width) / 100;

    form.style.transform = 'scale(' + scale + ')';
  }
}
